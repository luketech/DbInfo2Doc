package org.db.info.db.meta;

import java.util.ArrayList;
import java.util.List;

/**
 * 表信息
 */
public class TableMeta {

	private String dbType = "";
	private String dbName = "";
	private String tableName = "";
	private String primaryKey = "";
	private List<String> uniqueKeys = new ArrayList<>();
	private List<String> indexKeys = new ArrayList<>();
	private String remark = "";
	private List<ColumnMeta> columns = new ArrayList<>();
	
	public String getDbType() {
		return dbType;
	}
	public void setDbType(String dbType) {
		this.dbType = dbType;
	}
	public String getDbName() {
		return dbName;
	}
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getPrimaryKey() {
		return primaryKey;
	}
	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}
	public List<String> getUniqueKeys() {
		return uniqueKeys;
	}
	public void setUniqueKeys(List<String> uniqueKeys) {
		this.uniqueKeys = uniqueKeys;
	}
	public List<String> getIndexKeys() {
		return indexKeys;
	}
	public void setIndexKeys(List<String> indexKeys) {
		this.indexKeys = indexKeys;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public List<ColumnMeta> getColumns() {
		return columns;
	}
	public void setColumns(List<ColumnMeta> columns) {
		this.columns = columns;
	}
}
