package org.db.info;

import java.io.File;
import java.util.function.Predicate;
import org.db.info.db.DbBuilder;

public class Demo {

    private static String ip = "";
    private static int port = 3306;
    private static String user = "";
    private static String password = "";
    private static String url = "jdbc:mysql://%s:%s/%s?zeroDateTimeBehavior=convertToNull&useInformationSchema=true&characterEncoding=UTF-8";

    public static void main(String[] args) {
        ip = "127.0.0.1";
        port = 3306;
        user = "root";
        password = "123456";

        String outputBasePath = "D:\\";
        File baseFile = new File(outputBasePath);
        if (!baseFile.exists()) {
            baseFile.mkdirs();
        }

        try {
            File file = newFile(baseFile.getPath(), "数据库表结构信息导出");
            if (file != null) {
                new DbBuilder(String.format(url, ip, port, "test"), user, password)
                        .setSkipPredicate((Predicate<String>) tableName -> tableName.toLowerCase().startsWith("batch_"))
                        .render(file);
                System.out.println("生成成功!");
            } else {
                System.err.println("生成失败!");
            }
        } catch (Exception e) {
            System.err.println("生成失败!");
            e.printStackTrace();
        }
    }

    private static File newFile(String basePath, String fileName) {
        try {
            File file = new File(basePath + File.separator + fileName + ".html");
            if (!file.exists()) {
                file.createNewFile();
            }
            return file;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
