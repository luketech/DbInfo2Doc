# DbInfo2Doc

#### 介绍
参考JFinal自动生成的相关代码，基于JDBC和Enjoy模板引擎(JFinal enjoy template)实现的导出数据库中的表结构信息，方便在写数据库设计文档时自动生成，节省工作量。生成文件为HTML，可以浏览器打开复制到Word文档中。目前仅针对MySQL做了适配。

#### 使用说明

```
import java.io.File;
import org.db.info.db.DbBuilder;

public class Demo {
	
	public static void main(String[] args) {		
		try {
			File file = new File("D:\\表结构信息导出.html");
			new DbBuilder("jdbc:mysql://127.0.0.1:3306/mydb?zeroDateTimeBehavior=convertToNull&useInformationSchema=true", "root", "123456").render(file);
			System.out.println("生成成功!");
		} catch (Exception e) {
			System.err.println("生成失败!");
			e.printStackTrace();
		}
	}
}
```

#### 截图展示
![输入图片说明](https://images.gitee.com/uploads/images/2019/0508/101126_dd2899b4_58975.png "screenshot1.png")